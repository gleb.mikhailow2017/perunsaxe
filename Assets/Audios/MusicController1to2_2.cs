using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController1to2_2 : MonoBehaviour
{
    public AudioClip firstClip;
    public AudioClip loopClip;
    private AudioSource _audioSource;
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.loop = true;
        StartCoroutine(playEngineSound());
    }

    IEnumerator playEngineSound()
    {
        _audioSource.clip = firstClip;
        _audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        _audioSource.clip = loopClip;
        _audioSource.Play();
    }
}
