using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneInfo : MonoBehaviour
{   
    public static int nextScene; 
    private static string transferScene = "LoadScene";

    public static void StartScene(int scene)
    {
        nextScene = scene;
        SceneManager.LoadScene(transferScene);
    }
}
