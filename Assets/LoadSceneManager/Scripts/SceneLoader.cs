using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public Image progressBar;
    private AsyncOperation loadingSceneOperation;
    private void Awake()
    {
        loadingSceneOperation = SceneManager.LoadSceneAsync(SceneInfo.nextScene);
    }
    
    private void Update()
    {
        progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, loadingSceneOperation.progress,
            Time.deltaTime * 5);
    }
}
