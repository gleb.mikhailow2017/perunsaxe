using System.Collections;
using System.Collections.Generic;
using EmeraldAI;
using UnityEngine;

public class WeaponColliderEmeraldAI : MonoBehaviour
{
    [SerializeField] private WeaponBasic wBase;

    private void OnTriggerEnter(Collider other)
    {
        print(other.name + "    " + other.GetComponent<EmeraldAISystem>());
        if (other.GetComponent<EmeraldAISystem>())
        {
            other.GetComponent<EmeraldAISystem>().Damage(wBase.Damage, EmeraldAISystem.TargetType.AI, transform.root, 400);
        }
    }
}
