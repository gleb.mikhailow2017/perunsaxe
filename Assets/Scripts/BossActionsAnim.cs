using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossActionsAnim : MonoBehaviour
{
    [SerializeField] private BossYasher boss;
    
    public void Teleportation()
    {
        boss.SetNewPos();
    }

    public void SecondFase()
    {
        boss.SecondFase();
    }
}
