using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartTrigger : MonoBehaviour
{
    public BossYasher boss;
    public Animator door;

    private void OnTriggerEnter(Collider other)
    {
        door.SetTrigger("close");
        boss.StartFight();
        gameObject.SetActive(false);
        TextController.main.ShowText(0);
    }
}
