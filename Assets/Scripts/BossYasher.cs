using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EmeraldAI;
using MoreMountains.Feedbacks;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BossYasher : Damagable
{
    [SerializeField] private float maxHP;
    [SerializeField] GameObject Yasher;
    [SerializeField] private Transform[] spawns;
    [SerializeField] private Animator anim;

    [SerializeField] private Image boosBar, incBar;
    [SerializeField] bool isInc;
    [SerializeField] private List<GameObject> enemies;
    [SerializeField] private GameObject player;

    [SerializeField] private int state, maxState;
    public MMF_Player OnStart;
    private void Start()
    {
        state = maxState - 1;
        sepsis = sepsisPoints.Length;
        timeTraveler.SetActive(false);
        anim.gameObject.SetActive(false);
        boosBar.enabled = false;
        incBar.enabled = false;
        Alert.SetActive(false);
    }

    public override void DamageTaken(float dmg, string dmgType)
    {
        if(isInc) return;
        
        HP -= dmg;
        boosBar.fillAmount = HP / maxHP;
        
        
        if(HP <= 0)
        {
            GetComponent<Collider>().enabled = false;
            SecondFaseStart();
            boosBar.enabled = false;
            incBar.enabled = false;
            return;
        }
        
        if (HP <= maxHP * state / maxState)
        {
            anim.SetTrigger("attack");
            state--;
            SpawnYashers();
        }
    }

    public void StartFight()
    {
        anim.gameObject.SetActive(true);
        OnStart.PlayFeedbacks();
        anim.SetTrigger("start");
        StartCoroutine(ShowBar());
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(8f);
        OnSummonEnded();
    }
    
    public void OnSummonEnded()
    {
        SpawnYashers();
    }

    IEnumerator ShowBar()
    {
        boosBar.enabled = true;
        incBar.enabled = true;
        for (float i = 0; i < 0.6f; i += 0.02f)
        {
            boosBar.fillAmount = i / 0.6f;
            incBar.fillAmount = i / 0.6f;
            yield return new WaitForSeconds(0.02f);
        }

        incBar.fillAmount = 1;
        boosBar.fillAmount = 1;
    }
    
    public void SpawnYashers()
    {
        foreach (var s in spawns)
        {
            GameObject e = GameObject.Instantiate(Yasher);
            e.transform.position = s.position;
            enemies.Add(e);
        }
        isInc = true;
        incBar.enabled = true;
    }

    private void FixedUpdate()
    {
        if (!isInc) return;
        Check();
    }

    public void Check()
    {
        int deades = 0;
        for (int i = 0; i<enemies.Count; i++ )
        {
            if (enemies[i] == null || enemies[i].GetComponent<EmeraldAISystem>().IsDead ) deades++;
        }
        isInc = deades != enemies.Count;
        if (deades == enemies.Count)
        {
            incBar.enabled = false;
            enemies.Clear();
        }
    }
    
    [SerializeField] private int sepsis;
    [SerializeField] private GameObject[] sepsisPoints;
    [SerializeField] Transform SecondPosition;
    [SerializeField] private GameObject timeTraveler;

    public void SecondFaseStart()
    {
        TextController.main.ShowText(1);
        anim.SetTrigger("fase");
    }
    
    public void SetNewPos()
    {
        transform.position = SecondPosition.position;
    }
    
    public void SecondFase()
    {
        FindObjectOfType<station1tostation2audio>().StartSecond();
        timeTraveler.SetActive(true);
        foreach (var p in sepsisPoints)
        {
            p.SetActive(true);
        }
    }

    public void StartSecondAttack()
    {
        StartCoroutine(SpawnWalls());
        isSpawning = true;
    }

    [SerializeField] private GameObject wall;
    [SerializeField] private Transform wallStart;
    [SerializeField] private bool isSpawning;
    [SerializeField] private float dur;
    [SerializeField] private GameObject Alert, powerWall;
    

    IEnumerator SpawnWalls()
    {
        while (isSpawning)
        {
            yield return new WaitForSeconds(dur);
            int r = Random.Range(3, 5);
            for (int i = 0; i < r; i++)
            {
                Alert.SetActive(true);
                yield return new WaitForSeconds(0.05f);
                Alert.SetActive(false);
                yield return new WaitForSeconds(0.05f);
            }

            for (int i = 0; i < r; i++)
            {
                GameObject w = GameObject.Instantiate(wall);
                w.transform.position = wallStart.position;
                w.GetComponent<FireWall>().OnInstantiete();
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
    
    public void SepsisDeath()
    {
        sepsis--;
        if(sepsis <= 0) Final();
    }

    [SerializeField] private GameObject deadend;
    public void Final()
    {
        TextController.main.ShowText(3);
        deadend.SetActive(true);
    }
}
