using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Chains : Damagable
{
    [SerializeField] private Animator[] doors;
    
    public override void OnDeath()
    {
        foreach (var an in doors)
        {
            //an.SetTrigger("open")t
            an.gameObject.SetActive(false);
        }
        Destroy(gameObject);
    }
}
