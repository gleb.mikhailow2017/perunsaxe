using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ChangeMenu : MonoBehaviour
{
    public Animator anim;
    public LightWorking[] lights;
    
    private void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(SetMenu1());
    }

    IEnumerator SetMenu1()
    {
        yield return new WaitForSeconds(Random.Range(12f, 15f));
        anim.SetTrigger("old");
        StartCoroutine(SetMenu2());
    }

    IEnumerator SetMenu2()
    {
        yield return new WaitForSeconds(Random.Range(12f, 15f));
        anim.SetTrigger("sci");
        foreach (var VARIABLE in lights)
        {
            VARIABLE.Start();
        }
        StartCoroutine(SetMenu1());
    }
}
