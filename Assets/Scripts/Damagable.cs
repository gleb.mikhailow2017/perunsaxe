using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damagable : MonoBehaviour
{
    public float HP;
    
    public virtual void DamageTaken(float dmg, string dmgType)
    {
        HP -= dmg;
        
        if (HP <= 0) OnDeath();
    }

    public virtual void OnDeath()
    {
        
    }
}
