using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadEnd : Damagable
{
    public override void OnDeath()
    {
        GameManager.main.LoadScene(6);
        Destroy(gameObject);
    }
}
