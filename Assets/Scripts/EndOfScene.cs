using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndOfScene : MonoBehaviour
{
    public void LoadNestScene()
    {
        GameManager.main.LoadScene(2);
    }
}
