using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Lightbug.CharacterControllerPro.Implementation;
using Lightbug.Utilities;
using Lightbug.CharacterControllerPro.Core;
using UnityEditor;

public class AIBaseAttack : CharacterAIBehaviour
{
    public enum TypeEnemy
    {
        Sword,
        Archer,
        ReptilDog
    }

    public bool isAttacking;
    public Animator _animator;

    [Tooltip("The target transform used by the follow behaviour.")]
    public Transform followTarget = null;

    [Tooltip(
        "Desired distance to the target. if the distance to the target is less than this value the character will not move.")]
    [SerializeField]
    float reachDistance = 3f;

    [Tooltip("The wait time between actions updates.")] [Min(0f)] [SerializeField]
    float refreshTime = 0.65f;

    float timer = 0f;
    public float coefForwardOnAttack = 0.4f;

    NavMeshPath navMeshPath = null;
    public TypeEnemy typeEnemy = TypeEnemy.Sword;
    protected CharacterStateController stateController = null;
    public bool canMoveStoneman = true;
    private bool isDeath;
    protected override void Awake()
    {
        base.Awake();

        navMeshPath = new NavMeshPath();

        stateController = this.GetComponentInBranch<CharacterActor, CharacterStateController>();
        stateController.MovementReferenceMode = MovementReferenceMode.World;
    }

    public override void EnterBehaviour(float dt)
    {
        timer = refreshTime;
    }

    public override void UpdateBehaviour(float dt)
    {
        if (timer >= refreshTime)
        {
            timer = 0f;
            UpdateFollowTargetBehaviour(dt);
        }
        else
        {
            timer += dt;
        }
    }

    // Follow Behaviour --------------------------------------------------------------------------------------------------

    /// <summary>
    /// Sets the target to follow (only for the follow behaviour).
    /// </summary>
    public void SetFollowTarget(Transform followTarget, bool forceUpdate = true)
    {
        this.followTarget = followTarget;

        if (forceUpdate)
            timer = refreshTime + Mathf.Epsilon;
    }

    void UpdateFollowTargetBehaviour(float dt)
    {
        if (isDeath)
        {
            return;
        }
        if (GetComponentInParent<EnemyHealth>().health <= 0)
        {
            _animator.SetTrigger("Death");
            isDeath = true;
        }
        if (followTarget == null)
            return;
        
        if (typeEnemy == TypeEnemy.Archer && !canMoveStoneman)
        {
            SetMovementAction((Camera.main.transform.position -
                               GetComponentInParent<CharacterBody>().transform.position).normalized, 0.01f);
            if (!isAttacking)
            {
                Attack();
            }

            return;
        }

        characterActions.Reset();

        // NavMesh.CalculatePath(transform.position, followTarget.position, NavMesh.AllAreas, navMeshPath);

        if (navMeshPath.corners.Length < 2)
            return;

        Vector3 path = navMeshPath.corners[1] - navMeshPath.corners[0];

        bool isDirectPath = navMeshPath.corners.Length == 2;
        if (isDirectPath && path.magnitude <= reachDistance && !isAttacking)
        {
            Attack();
            SetMovementAction(Vector3.zero);
            return;
        }

        if (isAttacking)
        {
            if (typeEnemy == TypeEnemy.Archer)
            {
                
            }
            else if (typeEnemy == TypeEnemy.ReptilDog)
            {
                // Jump();
                // SetMovementAction(transform.forward, 2);
            }
            else
            {
                SetMovementAction(transform.forward, coefForwardOnAttack);
            }
        }

        if (!isAttacking && navMeshPath.corners.Length > 1)
            SetMovementAction(path);
    }

    protected void Attack()
    {
        isAttacking = true;
        _animator.SetTrigger("Attack");
        print("Attack");
    }

    public void StopAttack()
    {
        isAttacking = false;
        if (typeEnemy == TypeEnemy.Archer)
        {
            SetMovementAction(-transform.right, coefForwardOnAttack);
        }
    }
}