using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationAttackEvent : MonoBehaviour
{
    public AttackPlayerZone _attackPlayerZone;
    public AttackPlayerZone _secondAttackPlayerZone;
    public void Attacking()
    {
        print("Damage");
    }
    public void SecondAttacking()
    {
        _secondAttackPlayerZone.Damage();
    }
}
