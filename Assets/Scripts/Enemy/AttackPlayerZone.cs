using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AttackPlayerZone : MonoBehaviour
{
    public float damage = 10;
    public PlayerHealth trigger;

    public void Damage()
    {
        if (trigger)
        {
            trigger.Damage(damage);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerHealth>())
        {
            trigger = other.GetComponent<PlayerHealth>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        trigger = null;
    }
}