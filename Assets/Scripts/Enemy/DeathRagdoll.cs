using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lightbug.CharacterControllerPro.Core;
using UnityEngine;
using UnityEngine.AI;

public class DeathRagdoll : MonoBehaviour
{
    private List<Rigidbody> _rigidbodies;

    private void Awake()
    {
        _rigidbodies = GetComponentsInChildren<Rigidbody>().ToList();
    }

    private void Start()
    {
        foreach (var _rigidbody in _rigidbodies)
        {
            _rigidbody.isKinematic = true;
        }
    }
    public void Activate()
    {
        foreach (var _rigidbody in _rigidbodies)
        {
            _rigidbody.isKinematic = false;
        }
        Animator go = GetComponentInChildren<Animator>();
        GameObject g = go.gameObject;
        go.enabled = false;
        g.transform.parent = null;
        Destroy(gameObject);


    }
}
