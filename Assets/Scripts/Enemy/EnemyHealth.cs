using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lightbug.CharacterControllerPro.Core;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public float health = 10;
    public bool isDead;

    void Update()
    {
        isDead = health <= 0;
    }
    public void Damage(float damage)
    {
        if (!isDead)
        {
            if (health > 0)
            {
                health -= damage;
            }
            else
            {
                isDead = true;
                transform.DOScale(0, 3).OnComplete(D);
                return;
            }
        }
    }

    void D()
    {
        Destroy(gameObject);
    }
}
