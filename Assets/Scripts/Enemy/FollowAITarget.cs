using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowAITarget : MonoBehaviour
{
    [Tooltip("The target transform used by the follow behaviour.")] [SerializeField]
    Transform followTarget = null;

    [Tooltip(
        "Desired distance to the target. if the distance to the target is less than this value the character will not move.")]
    [SerializeField]
    float reachDistance = 3f;
    NavMeshPath navMeshPath = null;
    public Animator _animator;
    public bool isAttacking;
    void Start()
    {
        navMeshPath = new NavMeshPath();

    }

    // Update is called once per frame
    void Update()
    {
        if (followTarget == null)
            return;
        
        NavMesh.CalculatePath(transform.position, followTarget.position, NavMesh.AllAreas, navMeshPath);
        if (navMeshPath.corners.Length < 2)
            return;
        
        Vector3 path = navMeshPath.corners[1] - navMeshPath.corners[0];

        bool isDirectPath = navMeshPath.corners.Length == 2;
        if (isDirectPath && path.magnitude <= reachDistance && !isAttacking)
        {
            Attack();
            // SetMovementAction(Vector3.zero);
            return;
        }
    }

    void Attack()
    {
        
    }
}
