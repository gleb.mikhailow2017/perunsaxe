using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvizeDistance : MonoBehaviour
{
    public float visibleDistance = 30f;
    private SkinnedMeshRenderer _skinnedMeshRenderer;
    private Transform _target;
    private void Awake()
    {
        _skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        _target = FindObjectOfType<PlayerController>().transform;
    }

    private void Update()
    {
        if ((_target.position - transform.position).magnitude <= visibleDistance)
        {
            _skinnedMeshRenderer.material.SetFloat("_Alpha", 1 - (_target.position - transform.position).magnitude / visibleDistance);
        }
        else
        {
            _skinnedMeshRenderer.material.SetFloat("_Alpha", 0);
        }
    }
}
