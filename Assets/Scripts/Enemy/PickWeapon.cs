using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PickWeapon : MonoBehaviour
{
    public List<GameObject> weapons;
    public Transform parentWeapon;
    private void Awake()
    {
        Instantiate(weapons[Random.Range(0, weapons.Count)], parentWeapon);
    }
}
