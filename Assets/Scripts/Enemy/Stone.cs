using System;
using System.Collections;
using System.Collections.Generic;
using EmeraldAI.Example;
using Unity.VisualScripting;
using UnityEngine;

public class Stone : MonoBehaviour
{
    public int damage = 10;

    private void Start()
    {
        StartCoroutine(Delete());
    }

    IEnumerator Delete()
    {
        yield return new WaitForSeconds(10f);
        Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<EmeraldAIPlayerHealth>())
        {
            other.gameObject.GetComponent<EmeraldAIPlayerHealth>().DamagePlayer(damage);
        }
        StopCoroutine(Delete());
        Destroy(gameObject);
    }
}
