using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lightbug.CharacterControllerPro.Core;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class ThrowStone : MonoBehaviour
{
    public Stone stone;
    public Transform parentStone;
    public float forceThrow = 10f;
    
    void ThrowStoneAway()
    {
        parentStone.localScale = Vector3.zero;
        Stone throwStone = Instantiate(stone, parentStone.position, parentStone.rotation);
        Vector3 direction = ((Camera.main.transform.position -
                              transform.position) + new Vector3(0, 3, 0)) *
                            forceThrow;
        throwStone.GetComponent<Rigidbody>().AddForce(direction);
        parentStone.DOScale(1, 0.5f);
    }
}
