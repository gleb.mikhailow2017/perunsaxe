using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CaveEnter : MonoBehaviour
{
    public bool isOpen;

    private void Awake()
    {
        FindObjectOfType<SpawnWavesManager>().OnWavesEnded.AddListener(Open);
    }

    public void Open()
    {
        isOpen = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() && isOpen)
        {
            SceneTransition.SwitchToScene(4);
        }
    }
}
