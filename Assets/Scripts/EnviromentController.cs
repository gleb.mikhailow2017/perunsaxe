using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentController : MonoBehaviour
{
    [SerializeField] private Transform[] scifi, old;
    [SerializeField] private float changingTime;
    [SerializeField] private bool isOld, isChanging;

    public static EnviromentController main;

    private void Awake()
    {
        main = this;
    }

    public void Change()
    {
        if (isChanging) return;
        
        isOld = !isOld;
        isChanging = true;
        StartCoroutine(Changing());
    }

    IEnumerator Changing()
    {
        Inventory.main.ChanheTime();
        if (isOld)
        {
            foreach (var ob in old)
            {
                ob.gameObject.SetActive(true);
            }
            
            float t = changingTime;
            while (t>0)
            {
                foreach (var ob in scifi)
                {
                    ob.localScale = new Vector3(1, 1, 1) * t / changingTime;
                }
                foreach (var ob in old)
                {
                    ob.localScale = new Vector3(1, 1, 1) * (changingTime-t) / changingTime;
                }

                t -= 0.01f;
                yield return new WaitForSeconds(0.01f);
            }
            foreach (var ob in scifi)
            {
                ob.gameObject.SetActive(false);
            }
            foreach (var ob in old)
            {
                ob.localScale = new Vector3(1, 1, 1);
            }
        }
        else
        {
            foreach (var ob in scifi)
            {
                ob.gameObject.SetActive(true);
            }
            
            float t = changingTime;
            while (t>0)
            {
                foreach (var ob in old)
                {
                    ob.localScale = new Vector3(1, 1, 1) * t / changingTime;
                }
                foreach (var ob in scifi)
                {
                    ob.localScale = new Vector3(1, 1, 1) * (changingTime-t) / changingTime;
                }

                t -= 0.01f;
                yield return new WaitForSeconds(0.01f);
            }
            foreach (var ob in old)
            {
                ob.gameObject.SetActive(false);
            }
            foreach (var ob in scifi)
            {
                ob.localScale = new Vector3(1, 1, 1);
            }
        }
        isChanging = false;
    }
}
