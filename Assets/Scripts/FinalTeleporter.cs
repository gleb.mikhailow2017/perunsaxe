using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class FinalTeleporter : TimeTraveler
{
    [SerializeField] private BossYasher boss;
    public override void Interact()
    {
        TextController.main.ShowText(2);
        EnviromentController.main.Change();
        boss.StartSecondAttack();
        Destroy(gameObject);
        
    }
}
