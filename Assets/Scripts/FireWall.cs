
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWall : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;

    public float speed;
    public float lifeTime;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void OnInstantiete()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.back*speed;
    }

    IEnumerator LifeTime()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<PlayerHealth>().Damage(1);
    }
}
