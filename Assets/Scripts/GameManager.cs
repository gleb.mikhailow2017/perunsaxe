using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager main;
    public int currentscene;
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        main = this;
    }

    public void LoadScene(int ind)
    {
        currentscene = ind;
        SceneTransition.SwitchToScene(ind);
    }
}
