using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LightWorking : MonoBehaviour
{
    [SerializeField] private List<GameObject> lights;

    public void Start()
    {
        StartCoroutine(Wait(Random.Range(6f, 8f)));
    }
    
    IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        StartCoroutine((Blink(Random.Range(2, 5), Random.Range(0.01f, 0.03f))));
    }

    IEnumerator Blink(int count, float step)
    {
        for (int i = 0; i < count; i++)
        {
            foreach (var l in lights)
            {
                l.SetActive(false);
            }

            yield return new WaitForSeconds(0.01f);
            
            foreach (var l in lights)
            {
                l.SetActive(true);
            }
            
            yield return new WaitForSeconds(step);
        }

        if (Random.Range(0f, 1f) < 0.2f) StartCoroutine(off(Random.Range(0.3f, 0.7f) * (float)count));
        else StartCoroutine(Wait(Random.Range(6f, 8f)));

    }

    IEnumerator off(float time)
    {
        foreach (var l in lights)
        {
            l.SetActive(false);
        }

        yield return new WaitForSeconds(time);
            
        foreach (var l in lights)
        {
            l.SetActive(true);
        }

        StartCoroutine(Wait(Random.Range(6f, 8f)));
    }
}
