using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPlayer : MonoBehaviour
{
    public void Death()
    {
        Time.timeScale = 0f;
        StartCoroutine(DeathTimer());

    }

    IEnumerator DeathTimer()
    {
        TextController.main.ShowDeathText();
        yield return new WaitForSecondsRealtime(4f);
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
