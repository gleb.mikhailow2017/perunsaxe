using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lightbug.CharacterControllerPro.Core; //<-- Required
public class MovementProperty : MonoBehaviour
{
    public float speed = 10f;
    public float jumpForce = 10f;

    public KeyCode dashKeyCode = KeyCode.LeftShift;
    public KeyCode jumpKeyCode = KeyCode.Space;
    [SerializeField]
    CharacterActor characterActor = null;

    private void Awake()
    {
        characterActor = GetComponent<CharacterActor>();
    }

    private void Update()
    {
        Move();
        Jump();
    }

    private void Move()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");
        Vector3 move = new Vector3(v * speed, 0, h * speed);
        characterActor.Velocity = move;
    }

    private void Jump()
    {
        // print(_characterController.isGrounded);
        // if (_characterController.isGrounded && Input.GetKey(jumpKeyCode))
        // {
        //     _rigidbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Force);
        // }
    }
}
