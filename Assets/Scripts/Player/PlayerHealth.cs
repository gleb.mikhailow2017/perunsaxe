using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerHealth : MonoBehaviour
{
    public float health = 1;
    public bool isDead;
    public void Damage(float damage)
    { health -= damage; }

    public void Death()
    { }
}
