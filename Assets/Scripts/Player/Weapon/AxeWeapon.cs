using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.Feedbacks;
using Unity.VisualScripting;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine.UI;
using UnityEngine;
using Sequence = DG.Tweening.Sequence;

public class AxeWeapon : MeleeWeapon
{
    [SerializeField] private float seriesTime;
    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 startRot;

    [SerializeField] private Tween tween;
    [SerializeField] private WeaponStats stats;
    
    [SerializeField] private int canAttack => stats.canAttack;
    [SerializeField] private bool isAttacking => stats.isAttacking;
    [SerializeField] private int seriesNum => stats.seriesNum;

    public MMF_Player Attack1;
    public void Start()
    {
        stats = GetComponent<WeaponStats>();
        transform.localPosition = startPos;
        transform.localRotation = Quaternion.Euler(startRot);
        dmgColl.enabled = false;
    }

    
    public override void Attack()
    {
        if (canAttack == 3) return;
        stats.canAttack = 1;
        StopCoroutine(AttackSeriesTimer());
        StartCoroutine(AttackSeriesTimer());

        AttackAnim();
    }

    public void AttackAnim()
    {
        if((canAttack==2)||((canAttack==1)&&(seriesNum==0)))
        {
            switch (seriesNum)
            {
                case 0:
                    Anim1();
                    break;
                case 1:
                    Anim2();
                    break;
                case 2:
                    AnimRet();
                    break;
            }
            stats.seriesNum++;
        }
        else
        {
            if (!isAttacking)
            {
                AnimRet();
            }
        }
    }
    
    public void AttackSeriesAnim()
    {
        stats.canAttack++;
        AttackAnim();
    }
    
    void SetAttState()
    {
        stats.isAttacking = false;
        dmgColl.enabled = false;
    }
    
    
    public void Anim1()
    {
        stats.isAttacking = true;
        dmgColl.enabled = true;
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOLocalMove(endValue: new Vector3(-0.102740601f, 0.671570539f, 0.503777802f), duration: 0.1f))
            .Join(transform.DOLocalRotate(endValue: new Vector3(316.995361f, 343.87793f, 355.619507f), duration: 0.1f));
        if (Attack1)         Attack1.PlayFeedbacks();
        mySequence.Append(transform
                .DOLocalMove(endValue: new Vector3(-0.379673481f, 0.936611176f, 1.32848477f), duration: 0.3f))
            .Join(transform
                .DOLocalRotate(endValue: new Vector3(306.965454f, 47.5150108f, 214.564499f), duration: 0.3f)
                .OnComplete(SetAttState))
            .OnComplete(AttackSeriesAnim);
    }
    
    public void Anim2()
    {
        stats.isAttacking = true;
        dmgColl.enabled = true;
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOLocalMove(endValue: new Vector3(-0.379673481f,0.936611176f,1.32848477f), duration: 0.1f))
            .Join(transform.DOLocalRotate(endValue: new Vector3(56.0999184f,217.508224f,137.41008f), duration: 0.1f));

        mySequence.Append(transform
                .DOLocalMove(endValue: new Vector3(-0.129999995f,1.37f,0.330000013f), duration: 0.3f))
            .Join(transform
                .DOLocalRotate(endValue: new Vector3(71.0839767f,204.87764f,63.6089516f), duration: 0.3f)
                .OnComplete(SetAttState))
            .Append(transform.DOLocalRotate(endValue: new Vector3(71.0839767f,204.87764f,63.6089516f), duration: 0.1f))
            .OnComplete(AttackSeriesAnim);
    }
    
    public void AnimRet()
    {
        stats.seriesNum = 0;
        stats.canAttack = 0;
        stats.isAttacking = true;
        tween = transform.DOLocalMove(endValue: startPos, duration: 0.3f);
        tween = transform.DOLocalRotate(endValue: startRot, duration: 0.3f)
            .OnComplete(SetAttState);
    }
        
    IEnumerator AttackSeriesTimer()
    {
        yield return new WaitForSeconds(seriesTime);
        stats.canAttack = 0;
    }
}
