using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using EmeraldAI;

public class CrossbowWeapon : WeaponBasic
{
    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 startRot;
    [SerializeField] private float rayRange, dist;
    public Transform shootingPart;

    [SerializeField] private Tween tween;
    [SerializeField] private WeaponStats stats;
    [SerializeField] private LineRenderer line;
    [SerializeField] private LayerMask interactables;
    
    [SerializeField] private int canAttack => stats.canAttack;
    [SerializeField] private bool isAttacking => stats.isAttacking;
    [SerializeField] private int seriesNum => stats.seriesNum;
    
    public void Start()
    {
        transform.localPosition = startPos;
        transform.localRotation = Quaternion.Euler(startRot);
        stats = GetComponent<WeaponStats>();
        line.enabled = false;
    }

    public override void Attack()
    {
        RaycastHit hit;
        if (!isAttacking & (Physics.Raycast(transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, rayRange, interactables)))
        {
            if (hit.collider.gameObject.tag == "interactable" || hit.collider.gameObject.GetComponent<EmeraldAISystem>())
            {
                stats.isAttacking = true;
                stats.canAttack = 3;
                
                Vector3 newPos = transform.position + Camera.main.transform.forward * dist;
                float dis = (hit.transform.position - transform.position + Camera.main.transform.forward * dist).magnitude;
                line.enabled = true;
                
                line.SetPosition(0, shootingPart.position);
                tween = hit.transform.DOMove(transform.position + Camera.main.transform.forward * dist, dis/20f);
                StartCoroutine((DrawLine(hit.transform, dis / 20f)));
            }
        }
    }

    public IEnumerator DrawLine(Transform body, float time)
    {
        while (time > 0)
        {
            line.SetPosition(0, shootingPart.position);
            line.SetPosition(1, body.position);
            time -= 0.05f;
            yield return new WaitForSeconds(0.1f);
        }

        stats.isAttacking = false;
        stats.canAttack = 0;
        line.enabled = false;
    }
}
