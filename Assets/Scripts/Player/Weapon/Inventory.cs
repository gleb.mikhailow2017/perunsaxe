using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private List<GameObject> oldWeapons, newWeapons;
    [SerializeField] private bool isNew, isSecond;

    public static Inventory main;

    private void Awake()
    {
        main = this;
    }

    public void ChanheTime()
    {
        isNew = !isNew;
        if(isSecond) ChangeWeapon();
    }
    
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q)) ChangeWeapon(); 
    }

    public void ChangeWeapon()
    {
        if (oldWeapons[0].GetComponent<WeaponStats>().isAttacking) return;
        if (oldWeapons[1].GetComponent<WeaponStats>().isAttacking) return;
        if (newWeapons[0].GetComponent<WeaponStats>().isAttacking) return;
        if (newWeapons[1].GetComponent<WeaponStats>().isAttacking) return;
        isSecond = !isSecond;
        if (!isSecond)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.SetActive(false);

        }else if (!isNew)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(2).gameObject.SetActive(false);
            
        }else if (isNew)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.SetActive(true);
        }
    }
}
