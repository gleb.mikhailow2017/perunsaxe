using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PolearmWeapon : MeleeWeapon
{
    [SerializeField] private float seriesTime;
    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 startRot;

    [SerializeField] private Tween tween;
    [SerializeField] private WeaponStats stats;
    
    [SerializeField] private int canAttack => stats.canAttack;
    [SerializeField] private bool isAttacking => stats.isAttacking;
    [SerializeField] private int seriesNum => stats.seriesNum;

    public void Start()
    {
        stats = GetComponent<WeaponStats>();
        transform.localPosition = startPos;
        transform.localRotation = Quaternion.Euler(startRot);
        dmgColl.enabled = false;
    }

    
    public override void Attack()
    {
        if (canAttack == 3) return;
        stats.canAttack = 1;
        StopCoroutine(AttackSeriesTimer());
        StartCoroutine(AttackSeriesTimer());

        AttackAnim();
    }

    public void AttackAnim()
    {
        if((canAttack==2)||((canAttack==1)&&(seriesNum==0)))
        {
            switch (seriesNum)
            {
                case 0:
                    Anim1();
                    break;
                case 1:
                    Anim2();
                    break;
                case 2:
                    Anim3();
                    break;
                case 3:
                    AnimRet();
                    break;
            }
            stats.seriesNum++;
        }
        else
        {
            if (!isAttacking)
            {
                AnimRet();
            }
        }
    }
    
    public void AttackSeriesAnim()
    {
        stats.canAttack++;
        AttackAnim();
    }
    
    void SetAttState()
    {
        stats.isAttacking = false;
        dmgColl.enabled = false;
    }
    
    
    public void Anim1()
    {
        stats.isAttacking = true;
        dmgColl.enabled = true;
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform
                .DOLocalMove(endValue: new Vector3(-0.0199999996f,0.670000017f,0.280000001f), duration: 0.3f))
            .Join(transform
                .DOLocalRotate(endValue: new Vector3(287.832703f,311.205658f,317.392151f), duration: 0.3f)
                .OnComplete(SetAttState))
            .OnComplete(AttackSeriesAnim);
    }
    
    public void Anim2()
    {
        stats.isAttacking = true;
        dmgColl.enabled = true;
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOLocalMove(endValue: new Vector3(-0.0199999996f,0.670000017f,0.280000001f), duration: 0.1f))
            .Join(transform.DOLocalRotate(endValue: new Vector3(314.485931f,300f,62.1583405f), duration: 0.1f));

        mySequence.Append(transform
                .DOLocalMove(endValue: new Vector3(0.25f,0.540000021f,1.29999995f), duration: 0.3f))
            .Join(transform
                .DOLocalRotate(endValue: new Vector3(297.932861f,292.910736f,297.362976f), duration: 0.3f)
                .OnComplete(SetAttState))
            .Append(transform.DOLocalRotate(endValue: new Vector3(297.932861f,292.910736f,297.362976f), duration: 0.1f))
            .OnComplete(AttackSeriesAnim);
    }
    
    public void Anim3()
    {
        stats.isAttacking = true;
        dmgColl.enabled = true;
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform
                .DOLocalMove(endValue: new Vector3(0.202000007f,0.930000007f,0.349999994f), duration: 0.3f))
            .Join(transform
                .DOLocalRotate(endValue: new Vector3(299.644684f,338.935181f,308.416565f), duration: 0.3f)
                .OnComplete(SetAttState))
            .OnComplete(AttackSeriesAnim);
    }
    
    public void AnimRet()
    {
        stats.seriesNum = 0;
        stats.canAttack = 0;
        stats.isAttacking = true;
        tween = transform.DOLocalMove(endValue: startPos, duration: 0.3f);
        tween = transform.DOLocalRotate(endValue: startRot, duration: 0.3f)
            .OnComplete(SetAttState);
    }
        
    IEnumerator AttackSeriesTimer()
    {
        yield return new WaitForSeconds(seriesTime);
        stats.canAttack = 0;
    }
}
