using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using EmeraldAI;
using UnityEngine.PlayerLoop;

public class RifleWeapon : RangeWeapon
{
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 startRot;
    [SerializeField] private float rayRange;
    [SerializeField] private float damageMult, multPos;

    [SerializeField] private Tween tween;
    [SerializeField] private WeaponStats stats;
    [SerializeField] private LineRenderer line;
    [SerializeField] private LayerMask laserIntractables;
    
    [SerializeField] private bool isAttacking => stats.isAttacking;
    [SerializeField] private int seriesNum => stats.seriesNum;

    private KeyCode reload = KeyCode.R;
    
    public void Start()
    {
        transform.localPosition = startPos;
        transform.localRotation = Quaternion.Euler(startRot);
        stats = GetComponent<WeaponStats>();
        line.enabled = false;
        damageMult = curve.Evaluate(multPos);
    }

    public void Update()
    {
        if(Input.GetKeyDown(reload)) Reload();
    }

    public override void Attack()
    {
        if(isAttacking) return;
        if (Storage == 0) return;
        if(Pool == 0)
        { 
            Reload();
            return;
        }

        Storage--;
        Pool--;
        
        RaycastHit hit;
        GameObject m = new GameObject();
        stats.isAttacking = true;
        line.enabled = true;
        line.SetPosition(0, shootingPart.position);
        
        if (Physics.Raycast(transform.position,
                Camera.main.transform.TransformDirection(Vector3.forward), out hit, rayRange, laserIntractables))
        {
            m.transform.position = hit.point;
            if (hit.collider.gameObject.GetComponent<Damagable>())
            {
                hit.collider.gameObject.GetComponent<Damagable>().DamageTaken(Damage, DmgType);
            }

            
            multPos += 0.125f;
            damageMult = multPos <= 1 ? curve.Evaluate(multPos) : curve.Evaluate(1);
        }
        else
        {
            m.transform.position = Camera.main.transform.forward * rayRange + Camera.main.transform.position;
            multPos = 0;
            damageMult = curve.Evaluate(multPos);
        }
        StartCoroutine(DrawLine(m.transform));
    }

    public void Reload()
    {
        //anim reload
        if(Pool < MaxPool)
            Pool = Storage >= MaxPool ? MaxPool : Storage;
    }

    public IEnumerator DrawLine(Transform body)
    {
        float time = 0.2f;
        while (time > 0)
        {
            line.SetPosition(1, body.position);
            line.SetColors(new Color(1,.4f,.4f,time/0.2f), new Color(1,.3f,.3f,time/0.2f));
            time -= 0.02f;
            yield return new WaitForSeconds(0.02f);
        }

        stats.isAttacking = false;
        line.enabled = false;
        
        Destroy(body.gameObject);
    }
}
