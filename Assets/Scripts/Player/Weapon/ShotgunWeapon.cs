using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShotgunWeapon : RangeWeapon
{
    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 startRot;

    [SerializeField] private Tween tween;
    [SerializeField] private WeaponStats stats;
    [SerializeField] private ParticleSystem ps;
    
    [SerializeField] private int canAttack => stats.canAttack;
    [SerializeField] private bool isAttacking => stats.isAttacking;
    [SerializeField] private int seriesNum => stats.seriesNum;
    
    public void Start()
    {
        stats = GetComponent<WeaponStats>();
        ps = GetComponent<ParticleSystem>();
        dmgColl.enabled = false;
    }

    public override void Attack()
    {
        if (!isAttacking)
        {
            if (Storage > 0)
            {
                Storage -= 1;
                Shoot();
            }
        }
    }

    public void OnComplete()
    {
        dmgColl.enabled = true;
        ps.Play();
    }
    
    public void Shoot()
    {
        stats.isAttacking = true;
        stats.canAttack = 3;
        Sequence seq = DOTween.Sequence();

        seq.Append(transform.DOLocalMove(new Vector3(0, -0.0399999991f, 0.419999987f), 0.1f))
            .Join(transform.DOLocalRotate(new Vector3(344.819855f,0.0695548207f,346.259644f), 0.1f))
            .Append(transform.DOLocalMove(new Vector3(-0.639999986f,0.670000017f,0.230000004f), 0.3f))
            .Join(transform.DOLocalRotate(new Vector3(344.819885f,0.0695549175f,280.269958f), 0.3f))
            .Append(transform.DOLocalRotate(new Vector3(344.819885f,0.0695549175f,280.269958f), 0.1f))
            .OnComplete(OnComplete);
        
        seq = DOTween.Sequence();

        seq.Append(transform.DOLocalMove(new Vector3(-0.654999971f,0.727999985f,0.214000002f), 0.4f))
            .Join(transform.DOLocalRotate(new Vector3(344.819916f,0.0695531443f,286.289673f), 0.4f))
            .Append(transform.DOLocalMove(new Vector3(-0.639999986f, 0.670000017f, 0.230000004f), 0.4f))
            .Join(transform.DOLocalRotate(new Vector3(344.819885f, 0.0695549175f, 280.269958f), 0.4f))
            .OnComplete(AnimRet);
    }

    public void AnimRet()
    {
        dmgColl.enabled = false;
        stats.isAttacking = false;
        stats.canAttack = 0;
        tween = transform.DOLocalMove(startPos, 0.2f);
        tween = transform.DOLocalRotate(startRot, 0.2f);
    }
}
