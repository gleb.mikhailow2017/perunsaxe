using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class WeaponBasic : MonoBehaviour
{
    public int Damage;
    public string DmgType;
    public int MouseButton;
    public Collider dmgColl;

    public virtual void FixedUpdate()
    {
        if(Input.GetMouseButtonDown(MouseButton)) Attack();
    }

    public virtual void Attack()
    {
        
    }
}