using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollider : MonoBehaviour
{
    [SerializeField] private WeaponBasic wBase;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Damagable>())
        {
            other.GetComponent<Damagable>().DamageTaken(wBase.Damage, "");
        }
    }
}