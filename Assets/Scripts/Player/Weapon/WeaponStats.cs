using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStats : MonoBehaviour
{
    public int canAttack;
    public bool isAttacking;
    public int seriesNum;
}
