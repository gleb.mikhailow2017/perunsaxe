using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractor : MonoBehaviour
{
    [SerializeField] private GameObject UI;
    [SerializeField] private float dis;
    [SerializeField] private LayerMask interactableMask;

    private void Start()
    {
        UI.SetActive(false);
    }

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Camera.main.transform.TransformDirection(Vector3.forward),
                out hit, dis, interactableMask))
        {
            UI.SetActive(true);
            if (Input.GetKeyDown((KeyCode.E)))
            {
                hit.collider.gameObject.GetComponent<Interactable>().Interact();
            }
        }
        else
        {
            UI.SetActive(false);
        }
    }
}
