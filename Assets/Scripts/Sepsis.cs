
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sepsis : Damagable
{
    [SerializeField] private BossYasher boss;
    
    public override void DamageTaken(float dmg, string dmgType)
    {
        if (dmgType == "laser")
        {
            boss.SepsisDeath();
            gameObject.SetActive(false);
        }
    }
}
