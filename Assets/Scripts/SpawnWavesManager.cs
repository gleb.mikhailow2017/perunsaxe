using System;
using System.Collections;
using System.Collections.Generic;
using EmeraldAI;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[Serializable]
public class SpawnPoint
{
    public Transform point;
    public GameObject enemy;
}

[Serializable]
public class Wave
{
    public List<SpawnPoint> spawnPoints;
}

public class SpawnWavesManager : MonoBehaviour
{
    public Wave[] waves;
    public int waveNumber;
    public bool isWavesEnded;
    private List<GameObject> aliveEnemies = new List<GameObject>();
    public UnityEvent OnWavesEnded;

    private void Start()
    {
        SpawnWave();
        TextController.main.ShowText(0);
    }

    public void SpawnWave()
    {
        aliveEnemies.Clear();
        foreach (var spawnPoint in waves[waveNumber].spawnPoints)
        {
            print((spawnPoint.enemy != null) + " " + (spawnPoint.point.position != null));
            var tmp = Instantiate(spawnPoint.enemy, spawnPoint.point.position, Quaternion.identity);
            aliveEnemies.Add(tmp);
        }
    }

    private void Update()
    {
        if (isWavesEnded) return;
        if (aliveEnemies.TrueForAll(y => y == null || y.GetComponent<EmeraldAISystem>().IsDead))
        {
            if (waveNumber + 1 >= waves.Length)
            {
                isWavesEnded = true;
                OnWavesEnded.Invoke();
                TextController.main.ShowText(1);
                // TODO диалоги пируна о окончании яиц 
                return;
            }

            TextController.main.ShowText(Random.Range(2, 5));
            waveNumber += 1;
            SpawnWave();
        }
    }
}