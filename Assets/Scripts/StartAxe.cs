using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartAxe : Interactable
{
    public override void Interact()
    { 
        GameManager.main.LoadScene(3);
    }
}
