using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextAnim : MonoBehaviour
{
    
    [SerializeField] public string txt;
    [SerializeField] public float time;

    public void StartTexting()
    {
        StartCoroutine(Texting());
    }

    public void StopText()
    {
        StopCoroutine(Texting());
        gameObject.SetActive(false);
    }

    IEnumerator Texting()
    {
        TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
        text.text = "";
        for (int i = 0; i < txt.Length; i++)
        {
            text.text += txt[i];
            yield return new WaitForSecondsRealtime(time / txt.Length);
        }

        yield return new WaitForSecondsRealtime(3f);
        StopText();
    }
}
