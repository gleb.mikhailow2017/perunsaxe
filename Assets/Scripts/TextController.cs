using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TextController : MonoBehaviour
{
    [SerializeField] private List<string> texts;
    [SerializeField] private List<float> times;
    [SerializeField] TextAnim text_main;

    private List<string> deathText = new List<string>()
        { "Для носителя топора смерть не конец!", "Ты не можешь оконочить свой путь смертью!" };

    public static TextController main;

    public void Awake()
    {
        main = this;
    }

    public void ShowDeathText()
    {
        text_main.gameObject.SetActive(true);
        text_main.txt = deathText[Random.Range(0, deathText.Count - 1)];
        text_main.time = 1f;
        text_main.StartTexting();
    }

    public void ShowText(int ind)
    {
        if (ind >= texts.Count) return;
        text_main.gameObject.SetActive(true);
        text_main.txt = texts[ind];
        text_main.time = times[ind];
        text_main.StartTexting();
    }
}