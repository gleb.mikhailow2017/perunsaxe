using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    [SerializeField] private int sceneInd;
    private void OnTriggerEnter(Collider other)
    {
        GameManager.main.LoadScene(sceneInd);
    }
}
