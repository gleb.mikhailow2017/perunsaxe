using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class station1tostation2audio : MonoBehaviour
{
    public AudioClip firstAudioClip;
    public AudioClip SecondAudioClip;
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void StartSecond()
    {
        _audioSource.clip = SecondAudioClip;
        _audioSource.Play();
    }
}
